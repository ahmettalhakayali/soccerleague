# SoccerLeague

**Android Studio Project with Mock API Usage**

**Purpose :** 

App gets,fetchs and lists soccer team names by using Mock API data.

**Usage :**

- Build and see the team names as listed on the screen as listview apereance.
- Click the "Draw Fixture" butto to see the fixture below.
- Fixture can seen in the scrollable textview bottom of the screen.




_Released signed and generated APK format is in the "Release" folder._

_Codes and projects are in the "SoccerLeague.zip" file._

_Screenshots can seen in the "Screenshots" folder._

